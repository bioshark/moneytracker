package com.roly.moneytracker;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PaymentSelectionsTest {

    @Test
    void getAllPayments() {
        List<Payment> payments = PaymentSelections.getAllPayments();
        payments.stream()
            .map(Payment::getDate)
            .forEach(System.out::println);
    }

    @Test
    void getAllPaymentsForDay() {
        List<Payment> payments = PaymentSelections.getAllPaymentsForDay(LocalDate.of(2015, 11, 17));
        payments.stream()
            .map(Payment::getDate)
            .forEach(System.out::println);
        assertEquals(2, payments.size());
    }

    @Test
    void getAllPaymentForMonth() {
        List<Payment> payments = PaymentSelections.getAllPaymentsForMonth(Year.of(2015), Month.NOVEMBER);
        payments.stream()
            .map(Payment::getDate)
            .forEach(System.out::println);
    }
}