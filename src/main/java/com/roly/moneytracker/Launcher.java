package com.roly.moneytracker;

import com.roly.moneytracker.controllers.MoneyTrackerMain;

public class Launcher {
    public static void main(String[] args) {
        MoneyTrackerMain.main(args);
    }
}
