package com.roly.moneytracker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public enum DerbyConnectionManager {
    INSTANCE;

    private static String dbURL;
    private static Connection conn;

    Connection getConnection(String dbName) {
        if (conn == null) {
            dbURL = "jdbc:derby:" + System.getProperty("user.dir") + "/database/" + dbName + ";create=true";
            createConnection();
        }
        return conn;
    }

    private static void createConnection() {
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").getDeclaredConstructor().newInstance();
            conn = DriverManager.getConnection(dbURL);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void shutdown() {
        try {
            if (conn != null) {
                DriverManager.getConnection(dbURL + ";shutdown=true");
                conn.close();
                conn = null;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
