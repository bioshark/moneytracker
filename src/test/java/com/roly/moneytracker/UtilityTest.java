package com.roly.moneytracker;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import static com.roly.moneytracker.Utility.*;

import static org.junit.jupiter.api.Assertions.*;

class UtilityTest {

    @Test
    void dayIssWeekEndDay() {
        System.out.println(isWeekEndDay(LocalDate.of(2019, 2, 1)));
        assertFalse(isWeekEndDay(LocalDate.of(2019, 2, 1)));
    }
}