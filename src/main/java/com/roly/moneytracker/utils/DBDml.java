package com.roly.moneytracker.utils;

public enum DBDml {
    INSERT("insert into payment values (?, ?, ?, ?, ?, ?)"),
    UPDATE("update payment set amount = ?, payment_type = ?, payment_date = ?, description = ?, confirmed = ? where payment_id = ?"),
    CONFIRM_PAYMENT("update payment set confirmed = ? where payment_id = ?"),
    SELECT_ALL("select * from payment"),
    SELECT_FOR_DATE("select * from payment where payment_date = ? and payment_type = '" + PaymentType.DAILY_PAYMENT + "'"),
    SELECT_FOR_MONTH("select * from payment where year(payment_date) = ? and month(payment_date) = ?")
    ;

    private String dml;

    DBDml(String dml) {
        this.dml = dml;
    }

    public String getDml() {
        return dml;
    }
}
