package com.roly.moneytracker;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static com.roly.moneytracker.utils.Constants.DB_NAME;

class DerbyConnectionManagerTest {

    @Test
    void connectionTest() throws SQLException {
        DerbyConnectionManager instance = DerbyConnectionManager.INSTANCE;
        Connection conn = instance.getConnection(DB_NAME);
        boolean connWasOk = conn.isValid(100);
        instance.shutdown();
        assertTrue(connWasOk && conn.isClosed());
    }
}