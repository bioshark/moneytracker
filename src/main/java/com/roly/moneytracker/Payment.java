package com.roly.moneytracker;

import com.roly.moneytracker.utils.PaymentType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.UUID;

import static com.roly.moneytracker.utils.Constants.MINOR_UNIT;

public class Payment {
    private final int amountEuroCents;
    private final PaymentType type;
    private final LocalDate date;
    private final String description;
    private final String paymentId;
    private final Boolean paymentConfirmed;

    private Payment(String paymentId, PaymentType type, LocalDate date, int amount, String description, Boolean paymentConfirmed) {
        this.amountEuroCents = amount;
        this.type = type;
        this.date = date;
        this.description = description;
        this.paymentId = paymentId;
        this.paymentConfirmed = paymentConfirmed;
    }

    private Payment(PaymentType type, LocalDate date, int amount, String description) {
        this.amountEuroCents = amount;
        this.type = type;
        this.date = date;
        this.description = description;
        this.paymentId = UUID.randomUUID().toString();
        this.paymentConfirmed = false;
    }

    static Payment newInstance(PaymentType type, LocalDate date, int amount, String description) {
        return new Payment(type, date, amount, description);
    }

    static Payment newInstance(ResultSet rs) {
        try {
            return new Payment(rs.getObject(1).toString(),
                PaymentType.valueOf(rs.getObject(3).toString()),
                Utility.getDateFromString(rs.getObject(4).toString(), "YYYY-MM-DD"),
                Double.valueOf(rs.getDouble(2) * MINOR_UNIT).intValue(),
                rs.getObject(5).toString(),
                rs.getBoolean(6));
        } catch (SQLException e) {
            return null;
        }
    }

    PaymentType getType() {
        return type;
    }

    LocalDate getDate() {
        return date;
    }

    String getDescription() {
        return description;
    }

    public int getAmountEuroCents() {
        return amountEuroCents;
    }

    String getPaymentId() {
        return paymentId;
    }

    Boolean getPaymentConfirmed() {
        return paymentConfirmed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Payment))
            return false;
        Payment payment = (Payment) o;
        return amountEuroCents == payment.amountEuroCents &&
            type == payment.type &&
            date.equals(payment.date) &&
            description.equals(payment.description);
    }

    @Override
    public int hashCode() {
        int result = Integer.hashCode(amountEuroCents);
        result = 31 * result + (type == null ? 0 : type.hashCode());
        result = 31 * result + (date == null ? 0 : date.hashCode());
        result = 31 * result + (description == null ? 0 : description.hashCode());
        result = 31 * result + Boolean.hashCode(paymentConfirmed);
        return result;
    }

    @Override
    public String toString() {
        return "Payment{" +
            "paymentId='" + paymentId + '\'' +
            ", amount=" + amountEuroCents +
            ", type=" + type +
            ", date=" + date +
            ", description='" + description +
            ", date=" + paymentConfirmed + '\'' +
            '}';
    }
}
