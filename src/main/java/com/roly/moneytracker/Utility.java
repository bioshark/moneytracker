package com.roly.moneytracker;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static com.roly.moneytracker.utils.Constants.MINOR_UNIT;

public class Utility {

    static LocalDate getDateFromString(String date, String formatPattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatPattern);
        return LocalDate.parse(String.format(date, formatter));

    }

    static PreparedStatement setValues(PreparedStatement statement, Object... params) throws SQLException {
        int index = 0;
        for (Object param : params) {
            statement.setObject(++index, param);
        }
        return statement;
    }

    static double calculateAmountForPayments(List<Payment> payments) {
        int total = 0;
        for (Payment p : payments) {
            total += p.getAmountEuroCents();
        }
        return (double) total / MINOR_UNIT;
    }

    static double euroFromCents(int cents) {
        return (double) cents / MINOR_UNIT;
    }

    public static List<LocalDate> getActualDaysOfMonth(int year, int month) {
        List<LocalDate> days = new ArrayList<>();
        LocalDate firstDayOfMonth = LocalDate.of(year, month, 1);
        LocalDate lastDayOfMonth = getLastValidDayInMonth(LocalDate.of(year, month, 1).plusMonths(1).minusDays(1));
        LocalDate actualMonthStart = getLastValidDayInMonth(firstDayOfMonth.minusDays(1)).plusDays(1);
        long nrDays = ChronoUnit.DAYS.between(actualMonthStart, lastDayOfMonth);

        for (int i = 0; i <= nrDays; i++) {
            days.add(actualMonthStart.plusDays(i));
        }
        return days;
    }

    static boolean isWeekEndDay(LocalDate day) {
        switch (day.getDayOfWeek()) {
            case SATURDAY:
                return true;
            case SUNDAY:
                return true;
            default:
                return false;
        }
    }

    public static List<CompleteDay> completeDaysPerMonth(int year, int month) {
        List<LocalDate> daysOfMonth = getActualDaysOfMonth(year, month);
        List<CompleteDay> spendingDaysPerMonth = new LinkedList<>();

        for (LocalDate ld : daysOfMonth) {
            spendingDaysPerMonth.add(CompleteDay.of(ld));
        }
        return spendingDaysPerMonth;
    }


    static Map<LocalDate, List<Payment>> paymentsPerMonth(int year, int month) {
        List<LocalDate> daysOfMonth = getActualDaysOfMonth(year, month);
        Map<LocalDate, List<Payment>> paymentsPerMonth = new LinkedHashMap<>();
        for (LocalDate ld : daysOfMonth) {
            paymentsPerMonth.put(ld, PaymentSelections.getAllPaymentsForDay(ld));
        }
        return paymentsPerMonth;
    }


    private static LocalDate getLastValidDayInMonth(LocalDate day) {
        LocalDate newEndDay = day.minusDays(2);

        switch (day.getDayOfWeek()) {
            case SATURDAY:
                switch (newEndDay.getDayOfWeek()) {
                    case SATURDAY:
                        return newEndDay.minusDays(3);
                    case SUNDAY:
                        return newEndDay.minusDays(2);
                    default:
                        return newEndDay.minusDays(1);
                }
            case SUNDAY:
                switch (newEndDay.getDayOfWeek()) {
                    case SATURDAY:
                        return newEndDay.minusDays(4);
                    case SUNDAY:
                        return newEndDay.minusDays(3);
                    default:
                        return newEndDay.minusDays(2);
                }
            default:
                return newEndDay;
        }
    }

}
