package com.roly.moneytracker;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.roly.moneytracker.utils.Constants.*;
import static com.roly.moneytracker.utils.DBDml.*;
import static com.roly.moneytracker.Utility.setValues;

class PaymentDml {
    private static final Connection conn = DerbyConnectionManager.INSTANCE.getConnection(DB_NAME);

    private PaymentDml() {
    } // non-instantiatable

    static boolean insertPayment(Payment payment) {
        try {
            PreparedStatement statement = setValues(conn.prepareStatement(INSERT.getDml()), payment.getPaymentId(),
                payment.getAmountEuroCents() / MINOR_UNIT, payment.getType().toString(), payment.getDate().toString(), payment.getDescription(),
                payment.getPaymentConfirmed());
            statement.executeUpdate();
            statement.close();
            return true;
        } catch (SQLException e) {
            DerbyConnectionManager.INSTANCE.shutdown();
            System.out.println(e.getMessage());
            return false;
        }
    }

    static int updatePayment(String paymentId, Payment newPayment) {
        try {
            PreparedStatement statement = setValues(conn.prepareStatement(UPDATE.getDml()), newPayment.getAmountEuroCents() / MINOR_UNIT,
                newPayment.getType().toString(), newPayment.getDate().toString(), newPayment.getDescription(),
                newPayment.getPaymentConfirmed(), paymentId);
            int updatedRows = statement.executeUpdate();
            statement.close();
            return updatedRows;
        } catch (SQLException e) {
            DerbyConnectionManager.INSTANCE.shutdown();
            System.out.println(e.getMessage());
            return 0;
        }
    }

    static int confirmPayment(String paymentId, boolean status) {
        try {
            PreparedStatement statement = setValues(conn.prepareStatement(CONFIRM_PAYMENT.getDml()), status, paymentId);
            int updatedRows = statement.executeUpdate();
            statement.close();
            return updatedRows;
        } catch (SQLException e) {
            DerbyConnectionManager.INSTANCE.shutdown();
            System.out.println(e.getMessage());
            return 0;
        }
    }
}
