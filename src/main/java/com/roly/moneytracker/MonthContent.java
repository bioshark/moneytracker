package com.roly.moneytracker;

import java.time.Month;
import java.time.Year;
import java.util.List;

public class MonthContent {
    private Month month;
    private Year year;
    private List<Payment> payments;

    public MonthContent(Year year, Month month) {
        this.year = year;
        this.month = month;
    }

    public Year getYear() {
        return year;
    }

    public Month getMonth() {
        return month;
    }

    public List<Payment> getPayments() {
        return payments;
    }
}
