package com.roly.moneytracker;

import java.time.LocalDate;
import java.util.List;

import static com.roly.moneytracker.PaymentSelections.getAllPaymentsForDay;
import static com.roly.moneytracker.Utility.calculateAmountForPayments;
import static com.roly.moneytracker.Utility.isWeekEndDay;

public class CompleteDay {

    private final LocalDate theDay;
    private final boolean weekend;
    private final List<Payment> payments;

    private int estimationEuroCents;
    private boolean dayFinalized;
    private double profit;

    private CompleteDay(LocalDate day) {
        this.theDay = day;
        weekend = isWeekEndDay(theDay);
        payments = getAllPaymentsForDay(theDay);
    }

    public static CompleteDay of(LocalDate day) {
        return new CompleteDay(day);
    }

    public void setEstimationEuroCents(int estimationEuroCents) {
        this.estimationEuroCents = estimationEuroCents;
    }

    public void setDayFinalized(boolean dayFinalized) {
        this.dayFinalized = dayFinalized;
        if (dayFinalized) {
            calculateProfit();
        }
    }

    public LocalDate getTheDay() {
        return theDay;
    }

    public boolean isWeekend() {
        return weekend;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public int getEstimationEuroCents() {
        return estimationEuroCents;
    }

    public boolean isDayFinalized() {
        return dayFinalized;
    }

    public double getProfit() {
        return profit;
    }

    private void calculateProfit() {
        this.profit = this.estimationEuroCents - calculateAmountForPayments(this.payments);
    }

    @Override
    public String toString() {
        return "CompleteDay{" +
            "theDay=" + theDay +
            ", weekend=" + weekend +
            ", payments=" + payments +
            ", estimationEuroCents=" + estimationEuroCents +
            ", dayFinalized=" + dayFinalized +
            ", profit=" + profit +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CompleteDay))
            return false;
        CompleteDay that = (CompleteDay) o;
        for (Payment p : payments) {
            if (!that.payments.contains(p)) {
                return false;
            }
        }
        return weekend == that.weekend &&
            estimationEuroCents == that.estimationEuroCents &&
            dayFinalized == that.dayFinalized &&
            Double.compare(that.profit, profit) == 0 &&
            theDay.equals(that.theDay) &&
            payments.equals(that.payments);
    }

    @Override
    public int hashCode() {
        int result = theDay == null ? 0 : theDay.hashCode();
        result = 31 * result + Boolean.hashCode(weekend);
        result = 31 + result + Integer.hashCode(estimationEuroCents);
        result = 31 * result + Boolean.hashCode(dayFinalized);
        result = 31 * result + Double.hashCode(profit);
        for (Payment p : payments) {
            result = 31 * result + p.hashCode();
        }
        return result;

    }
}
