create table PAYMENT (
  payment_id   varchar(36) PRIMARY KEY,
  amount       decimal(11, 2),
  payment_type varchar(20),
  payment_date date,
  description  varchar(200),
  confirmed    boolean
);