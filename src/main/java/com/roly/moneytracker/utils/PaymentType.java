package com.roly.moneytracker.utils;

public enum PaymentType {
    DAILY_PAYMENT,
    FIXED_SPENDING
}