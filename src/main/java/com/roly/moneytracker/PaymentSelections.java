package com.roly.moneytracker;

import com.roly.moneytracker.utils.DBDml;
import com.roly.moneytracker.utils.ResultSetProcessor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.roly.moneytracker.utils.Constants.DB_NAME;
import static com.roly.moneytracker.Utility.setValues;

class PaymentSelections {
    private static final Connection conn = DerbyConnectionManager.INSTANCE.getConnection(DB_NAME);

    private PaymentSelections() {
    } // non-instantiatable

    private static void applyProcessorOnSelection(DBDml select, ResultSetProcessor processor, Object... params) {
        try {
            PreparedStatement statement = setValues(conn.prepareStatement(select.getDml()), params);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                processor.process(rs);
            }
            statement.close();
        } catch (SQLException e) {
            DerbyConnectionManager.INSTANCE.shutdown();
            System.out.println(e.getMessage());
        }
    }

    static List<Payment> getAllPayments() {
        List<Payment> payments = new ArrayList<>();
        applyProcessorOnSelection(DBDml.SELECT_ALL,
            resultSet -> payments.add(Payment.newInstance(resultSet))
        );
        return payments;
    }

    static List<Payment> getAllPaymentsForDay(LocalDate localDate) {
        List<Payment> payments = new ArrayList<>();
        applyProcessorOnSelection(DBDml.SELECT_FOR_DATE,
            resultSet -> payments.add(Payment.newInstance(resultSet)),
            Objects.requireNonNull(localDate).toString()
        );
        return payments;
    }

    static List<Payment> getAllPaymentsForMonth(Year year, Month month) {
        List<Payment> payments = new ArrayList<>();
        applyProcessorOnSelection(DBDml.SELECT_FOR_MONTH,
            resultSet -> payments.add(Payment.newInstance(resultSet)),
            year.getValue(), month.getValue()
        );
        return payments;
    }
}
