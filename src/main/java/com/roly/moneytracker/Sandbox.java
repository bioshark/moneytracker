package com.roly.moneytracker;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static com.roly.moneytracker.utils.PaymentType.DAILY_PAYMENT;
import static com.roly.moneytracker.utils.PaymentType.FIXED_SPENDING;
import static com.roly.moneytracker.Utility.*;

public class Sandbox {
    private static final DerbyConnectionManager connectionManager = DerbyConnectionManager.INSTANCE;

    public static void main(String[] args) {
        System.out.println(FIXED_SPENDING);
        Payment payment = Payment.newInstance(DAILY_PAYMENT, LocalDate.of(2015, 11, 17), 4454, "subway");
//        System.out.println(payment);

//        if (PaymentDml.insertPayment(payment)) {
//            System.out.println("done;");
//        }

//        System.out.println(PaymentDml.updatePayment("ee0e7fc8-6c65-4340-9433-4709e35090bf", payment));
//
//        System.out.println(PaymentDml.confirmPayment("d0112946-8940-4040-b18c-228426eab7f6", false));

//        List<Payment> payments = PaymentSelections.getAllPaymentsForDay(LocalDate.of(2015, 11, 17));
//
//        System.out.println("Payments in the DB are: ");
//        for (Payment p : payments) {
//            System.out.println(p);
//        }
//        System.out.println("Total = " + Utility.calculateAmountForPayments(payments));

        // display payments for month
//        displayPaymentsForMonth();

//        displayCompleteDaysPerMonth(2015, 11);
        List<LocalDate> days = getActualDaysOfMonth(2019, 02);
        days.stream()
            .forEach(System.out::println);

        connectionManager.shutdown();

    }

    private static void displayPaymentsForMonth() {
        Map<LocalDate, List<Payment>> paymentsPerMonth = paymentsPerMonth(2015, 11);
        paymentsPerMonth.forEach((k, v) -> {
            System.out.print(k + " : ");
            v.forEach(s -> System.out.print(s.getAmountEuroCents() + "; "));
            System.out.println();
        });
    }

    private static void displayCompleteDaysPerMonth(int year, int month) {
        List<CompleteDay> completeDays = completeDaysPerMonth(year, month);
        completeDays.forEach(System.out::println);
    }

}