package com.roly.moneytracker.controllers;

import com.roly.moneytracker.CompleteDay;
import com.roly.moneytracker.DerbyConnectionManager;

import java.time.LocalDate;
import java.util.List;

import static com.roly.moneytracker.Utility.getActualDaysOfMonth;

public class MoneyTrackerMainController {
    private MoneyTrackerMain main;

    private static final DerbyConnectionManager connectionManager = DerbyConnectionManager.INSTANCE;
    private List<CompleteDay> completeDays;


    void setMain(MoneyTrackerMain main) {
        this.main = main;
//        initializeMonth();
    }

    static void initializeMonth() {
        List<LocalDate> days = getActualDaysOfMonth(2019, 2);
        LocalDate firstDay = days.stream()
            .sorted().findFirst().orElse(LocalDate.now());
        System.out.println("Initial: " + firstDay);
        while(!firstDay.getDayOfWeek().toString().equals("MONDAY")) {
            firstDay = firstDay.minusDays(1);
        }
        System.out.println("After: " + firstDay);
    }
}
