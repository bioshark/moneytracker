package com.roly.moneytracker.controllers;

import com.roly.moneytracker.CompleteDay;
import com.roly.moneytracker.DerbyConnectionManager;
import com.roly.moneytracker.Payment;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.List;

import static com.roly.moneytracker.Utility.completeDaysPerMonth;

public class TestWindow extends Application {
    private static final DerbyConnectionManager connectionManager = DerbyConnectionManager.INSTANCE;

    @Override
    public void start(Stage stage) {
        Group root = new Group();

        List<CompleteDay> completeDays = completeDaysPerMonth(2015, 11);
        int y = 20;
        int newX = 20;
        for (CompleteDay cd : completeDays) {
            newX += 58;
            Rectangle square = new Rectangle(20, y, newX, 20);
            if (cd.isWeekend()) {
                square.setFill(Color.GRAY);
            } else {
                square.setFill(Color.LIGHTGRAY);
            }
            square.setStroke(Color.BROWN);
            square.setStrokeWidth(3);
            root.getChildren().add(square);

            y += 20;
            newX += 20;
            Text dayText = new Text(20, y - 2, cd.getTheDay().toString());
            root.getChildren().add(dayText);

            if (cd.getPayments().size() > 0) {
                for (Payment p : cd.getPayments()) {
                    Text value = new Text(newX, y - 2, p.getAmountEuroCents() + ";");
                    root.getChildren().add(value);
                    newX += Integer.toString(p.getAmountEuroCents()).length() * 9;
                }
            }
            newX = 20;
        }

        Scene scene = new Scene(root, 450, 100 + y, Color.LIGHTYELLOW);
        stage.setTitle("Month");
        stage.setScene(scene);
        stage.show();

    }

    @Override
    public void stop() {
        connectionManager.shutdown();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
